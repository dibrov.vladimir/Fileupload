package com.dibrova.fileupload.repository;

import com.dibrova.fileupload.model.FileUploadMetaData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileUploadRepository extends JpaRepository<FileUploadMetaData, Long> {
}
